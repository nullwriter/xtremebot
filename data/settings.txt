{
    "start_at_open_bot": 1,
    "thread_count": 1,
    "delay_from": 1,
    "delay_to": 5,
    "captcha_service": "2captcha",
    "show_browser": 1,
    "browser": "firefox",
    "continuos_run_mode": 1,
    
    "captcha_solver":{
        "max_retry_find_elements": 2,
        "strict_vote_check": 0,
        "retry_elements_delay": 0.3
    },
    
    "mass_voting":{

        "use_proxy": 0,

        "url": "http://www.xtremetop100.com/in.php?site=1132279624",

        "page_load_timeout": 4,
        "page_timeout_after_click_vote": 4,

        "timeout_after_click_vote_active": 0,

        "max_retry_proxy": 3,
        "reload_page_404_tries": 2,

        "browser":{
            "use_firefox_profile": 0
        }

    },

    "full_debug": 1
}