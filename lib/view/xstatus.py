from Tkinter import *
import logging
import ttk as tk
import datetime
import time
from ScrolledText import ScrolledText

class XStatusFrame:

    def __init__(self, master, window):
        self.frame = Frame(master, relief="raised", width=950)
        self.frame.grid(row=4,column=0,sticky=(E,W),columnspan=20,rowspan=2)

        self.clear_btn = Button(self.frame,text="Clear Log", command=lambda: self.clear_log())
        self.clear_btn.pack(side=TOP, anchor=W)

        # text widget
        self.mytext = ScrolledText(self.frame, undo=True,state="disabled", fg="white", bg="black")
        #self.mytext = Text(self.frame, state="disabled", fg="white", bg="black")
        self.mytext.pack(fill=X)

        # Create textLogger
        text_handler = TextHandler(self.mytext)

        # Add the handler to logger
        self.logger = logging.getLogger()
        self.logger.addHandler(text_handler)

    def clear_log(self):
        self.mytext.configure(state='normal')
        self.mytext.delete(1.0, END)
        self.mytext.configure(state='disabled')



class TextHandler(logging.Handler):
    """This class allows you to log to a Tkinter Text or ScrolledText widget"""
    def __init__(self, text):
        # run the regular Handler __init__
        logging.Handler.__init__(self)
        # Store a reference to the Text it will log to
        self.text = text

    def num_lines(self):
        return int(self.text.index('end').split('.')[0]) - 1

    def emit(self, record):
        msg = self.format(record)
        def append():
            self.text.configure(state='normal')
            self.text.insert(END, "["+self.timestamp()+"] "+msg + '\n')
            self.text.configure(state='disabled')
            # Autoscroll to the bottom
            self.text.yview(END)
        # This is necessary because we can't modify the Text from other threads
        self.text.after(0, append)

        try:
            if self.num_lines() > 300:
                self.text.configure(state='normal')
                self.text.delete(1.0, 300.0)
                self.text.configure(state='disabled')
        except Exception as e:
            print "(non fatal) exception clearing log: "+str(e)

    def timestamp(self):
        ts = time.time()
        return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')