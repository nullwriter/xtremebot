from Tkinter import *
import threading
import ttk
from lib.view.xtopmenu import XTopMenu
from lib.view.xaccount import AccountFrame
from lib.view.xvoting import VotingFrame
from lib.view.xstatus import XStatusFrame
import os
from sys import platform as _platform

class Window(threading.Thread):
    
    def __init__(self, version, queue, config_vote, app):
        threading.Thread.__init__(self)
        
        self.root = Tk()
        #self.root.geometry("1100x600")
        #self.root.resizable(width=FALSE, height=FALSE)
        self.root.wm_title("XtremeBot - v "+version)
        self.bot_version = version
        self.root.grid_columnconfigure(0, weight=1)

        self.config_vote = config_vote
        self.queue = queue
        self.app = app

        """
        Make the GUI window on top of everything
        """
        curr_system = self.get_os()
        if "windows" in curr_system:
            self.root.attributes("-topmost", True)
        else:
            os.system('''/usr/bin/osascript -e 'tell app "Finder" to set frontmost of process "Python" to true' ''')

        
        # top menu 
        self.menuframe = XTopMenu(self.root, self)

        # (log) status frame
        self.statusframe = XStatusFrame(self.root, self)

        self.container = Frame(self.root)
        self.container.grid(row=3, column=0, sticky=W+E)

        # voting frame
        self.voting_frame = VotingFrame(self.container, self, queue=self.queue, root=self.root)
        self.voting_frame.set_logger(self.statusframe.logger)

        # account frame
        self.account_frame = AccountFrame(self.container, self)

        #  frames (content)
        self.frames = {}
        for f in (self.account_frame,self.voting_frame): # defined subclasses of BaseFrame
            f.grid(row=3, column=0)
            self.frames[f] = f
        self.show_frame("Voting")

    def set_queue(self,q):
        self.queue = q

    def show_frame(self,str):
        if str == "Account":
            self.frames[self.account_frame].show()
            self.frames[self.voting_frame].grid_forget()
        elif str == "Voting":
            self.frames[self.voting_frame].show()
            self.frames[self.account_frame].grid_forget()

    def get_os(self):
        platform = "osx"

        if _platform == "linux" or _platform == "linux2":
            platform = "linux"
        elif _platform == "darwin":
            platform = "osx"
        elif _platform == "win32":
            platform = "windows"

        return platform


        