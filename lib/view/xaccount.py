import Tkinter as tk
from lib.view.xbaseframe import BaseFrame
from lib.model.xdata import XData

class AccountFrame(BaseFrame):
    
    def show(self):
        self.grid(row=3, column=0)
        self.tkraise()

    def create_widgets(self):

        ##########  ADD A PROXY  ##########

        self.proxy_add_frame = tk.Frame(self,relief=tk.SUNKEN)
        self.proxy_add_frame.grid(row=0,column=0,padx=10,pady=10,sticky=tk.N+tk.W)

        labelframe = tk.LabelFrame(self.proxy_add_frame, text="Add New Proxy", padx=5, pady=5)
        labelframe.grid(row=0,column=0,padx=10,pady=10)

        labelproxy_name = tk.Label(labelframe,text="Proxy *")
        labelproxy_name.grid(row=0,column=0)

        self.proxy_name = tk.Entry(labelframe)
        self.proxy_name.grid(row=0,column=1)

        labelproxy_port = tk.Label(labelframe,text="Port *")
        labelproxy_port.grid(row=0,column=2)

        self.proxy_port = tk.Entry(labelframe)
        self.proxy_port.grid(row=0,column=3)

        labelproxy_user = tk.Label(labelframe,text="User")
        labelproxy_user.grid(row=1,column=0)

        self.proxy_user = tk.Entry(labelframe)
        self.proxy_user.grid(row=1,column=1)

        labelproxy_pass = tk.Label(labelframe,text="Password")
        labelproxy_pass.grid(row=1,column=2)

        self.proxy_pass = tk.Entry(labelframe)
        self.proxy_pass.grid(row=1,column=3)

        self.add_proxy_btn = tk.Button(labelframe, text="Add Proxy")
        self.add_proxy_btn.grid(row=2,column=0,columnspan=6)


        ##########  PROXY MANAGER  ##########

        self.proxy_manager = tk.Frame(self,relief=tk.SUNKEN)
        self.proxy_manager.grid(row=0,column=1,padx=10,pady=10,sticky=tk.N+tk.E,rowspan=6)

        labelframe_proxy_man = tk.LabelFrame(self.proxy_manager, text="Proxy Manager", padx=10, pady=10)
        labelframe_proxy_man.grid(row=0,column=0,padx=10,pady=10)

        self.proxy_list = tk.Listbox(labelframe_proxy_man)
        self.proxy_list.grid(row=0,column=0,rowspan=12)

        #for item in ["one", "two", "three", "four", "bleh", "blu","lele","hello","an ip","another","thing"]:
            #self.proxy_list.insert(tk.END, item)

        xdata = XData()
        for item in xdata.get_proxies():
            self.proxy_list.insert(tk.END, item)

        self.rm_proxy_btn = tk.Button(labelframe_proxy_man, text="Delete Selected")
        self.rm_proxy_btn.grid(row=0,column=1, sticky=tk.E)

        self.rm_all_proxy_btn = tk.Button(labelframe_proxy_man, text="Delete All Proxies")
        self.rm_all_proxy_btn.grid(row=1,column=1, sticky=tk.E)


        ##########  PROXY IMPORT/EXPORT  ##########

        self.proxy_load = tk.Frame(self,relief=tk.SUNKEN)
        self.proxy_load.grid(row=1,column=0,padx=10,pady=10,sticky=tk.N+tk.W)

        labelframe_proxy_load = tk.LabelFrame(self.proxy_load, text="Import / Export Proxies", padx=10, pady=10)
        labelframe_proxy_load.grid(row=0,column=0,padx=10,pady=10)

        self.import_proxy_btn = tk.Button(labelframe_proxy_load, text="Import Proxies")
        self.import_proxy_btn.grid(row=0,column=0, sticky=tk.W)

        self.export_proxy_btn = tk.Button(labelframe_proxy_load, text="Export Proxies")
        self.export_proxy_btn.grid(row=0,column=1, sticky=tk.W)
