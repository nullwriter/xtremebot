import Tkinter as tk

class BaseFrame(tk.Frame):
    """An abstract base class for the frames that sit inside PythonGUI.

    Args:
      master (tk.Frame): The parent widget.
      controller (PythonGUI): The controlling Tk object.

    Attributes:
      controller (PythonGUI): The controlling Tk object.

    """

    def __init__(self, master, controller, queue=None, root=None):
        tk.Frame.__init__(self, master)
        self.controller = controller
        self.grid()
        self.create_widgets()
        self.root = root
        self.queue = queue

    def create_widgets(self):
        """Create the widgets for the frame."""
        raise NotImplementedError

    def show(self):
        raise NotImplementedError