from Tkinter import *
from lib.view.xaccount import AccountFrame
from lib.view.xvoting import VotingFrame

class XTopMenu:
    
    def __init__(self, master, window):

        self.frame = Frame(master, pady=10 ,bg="black", relief="raised", width=950)
        self.frame.grid(row=0,column=0,sticky=(E,W),columnspan=10,rowspan=2)

        if window.app.current_user.get_type() == "admin":

            self.account = Button(self.frame,text="Account Management",state="normal",
                                  activebackground="#f26450" , activeforeground="white",
                                  command=lambda: window.show_frame("Account"))
            self.account.grid(row=0,column=0, sticky=(N,S,E,W),padx=40)

            self.voting = Button(self.frame, text="Mass Voting",command=lambda: window.show_frame("Voting"))
            self.voting.grid(row=0,column=2, sticky=(N,S,E,W),padx=40)

            self.settings = Button(self.frame, text="General Settings")
            self.settings.grid(row=0,column=3, sticky=(N,S,E,W),padx=40)

            self.ptester = Button(self.frame, text="Proxy Tester")
            self.ptester.grid(row=0,column=10, sticky=(N,S,E,W),padx=40)

        else:

            self.voting = Button(self.frame, text="Mass Voting",command=lambda: window.show_frame("Voting"))
            self.voting.grid(row=0,column=2, sticky=(N,S,E,W),padx=40)
