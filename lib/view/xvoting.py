import Tkinter as tk
from lib.view.xbaseframe import BaseFrame
from lib.controller.xprocess import XProcess
from threading import Thread
from lib.model.xdata import XData
from lib.model.xconnector import XConnectDB
import Queue

class VotingFrame(BaseFrame):

    def set_logger(self,logger):
        self.logger = logger

    def show(self):
        self.grid(row=3, column=0)
        self.tkraise()
        
    def create_widgets(self):

        self.current_user = self.controller.app.current_user
        self.conn = self.controller.app.conn
        self.xdata = XData()

        ##########  MASS PROMOTER SETTINGS  ##########

        self.voting_settings_frame = tk.Frame(self,relief=tk.SUNKEN)
        self.voting_settings_frame.grid(row=0,column=0,padx=10,pady=10,sticky=tk.N+tk.W)

        labelframe = tk.LabelFrame(self.voting_settings_frame, text="Mass Promoter Settings", padx=5, pady=5)
        labelframe.grid(row=0,column=0,padx=10,pady=10)

        # Vote URL

        if self.current_user.get_type() == "admin":

            labelsite_voteurl = tk.Label(labelframe,text="Site Vote's URL (1 per line):")
            labelsite_voteurl.grid(row=0,column=0,sticky=tk.W,columnspan=5)

            self.vote_url = tk.Text(labelframe,bg='white',height=5,insertborderwidth=2)
            self.vote_url.insert(tk.END, self.xdata.get_vote_url())
            self.vote_url.grid(row=1,column=0,columnspan=8)

        else:
            label_credits = tk.Label(labelframe, text="Credits Available: ")
            label_credits.grid(row=0, column=0, sticky=tk.W, columnspan=5)

            self.label_credits_num = tk.Label(labelframe, text=""+str(self.current_user.get_available_credits()))
            self.label_credits_num.grid(row=0, column=1, sticky=tk.W, columnspan=5)

        # Choose Browser
        label_browser = tk.Label(labelframe, text="Browser To Use: ")
        label_browser.grid(row=2, column=0, sticky=tk.W, columnspan=1)

        self.browser_chosen = tk.StringVar()

        browsers_choose = ('Firefox', 'Chrome')
        self.option_browser = tk.OptionMenu(labelframe, self.browser_chosen, *browsers_choose)
        self.browser_chosen.set('Firefox')
        self.option_browser.grid(row=2, column=1, sticky=tk.W, columnspan=8)

        # Choose Captcha
        if self.current_user.get_type() == "admin":
            label_captcha_api = tk.Label(labelframe, text="Captcha API to Use: ")
            label_captcha_api.grid(row=3, column=0, sticky=tk.W, columnspan=1)

        self.captcha_chosen = tk.StringVar()

        captcha_choose = ('2Captcha', 'Antigate', 'ImageTyperz')
        self.option_captcha_api = tk.OptionMenu(labelframe, self.captcha_chosen, *captcha_choose)
        self.captcha_chosen.set('2Captcha')

        if self.current_user.get_type() == "admin":
            self.option_captcha_api.grid(row=3, column=1, sticky=tk.W, columnspan=8)

        # Restart al proxies checkbox
        self.autorestart_flag = tk.IntVar()
        self.autorestart_flag.set(0)
        self.restar_proxies = tk.Checkbutton(labelframe, text="Restart All Proxies Once Processed (Continuos Run Mode)", onvalue=1,offvalue=0,variable=self.autorestart_flag)
        self.restar_proxies.grid(row=4,column=0,sticky=tk.W,columnspan=8)

        # Delay between actions
        self.label_delay = tk.Label(labelframe, text="Delay Between Actions:")
        self.label_delay.grid(row=5,column=0,sticky=tk.W)

        self.delay_a = tk.Entry(labelframe)
        self.delay_a.insert(tk.END,"10")
        self.delay_a.grid(row=5,column=1,sticky=tk.W,columnspan=1)

        tmplabel = tk.Label(labelframe, text="To:")
        tmplabel.grid(row=5,column=2,sticky=tk.W)

        self.delay_b = tk.Entry(labelframe)
        self.delay_b.insert(tk.END,"20")
        self.delay_b.grid(row=5,column=3,sticky=tk.W,columnspan=1)

        # Actions to send
        self.label_actions = tk.Label(labelframe, text="Actions To Send:")
        self.label_actions.grid(row=6,column=0,sticky=tk.W)

        self.actions_a = tk.Entry(labelframe)
        self.actions_a.grid(row=6,column=1,sticky=tk.W,columnspan=1)

        self.unliminted_act_flag = tk.IntVar()
        self.unliminted_act_flag.set(1)
        self.unlimited_act = tk.Checkbutton(labelframe, text="Unlimited Actions", onvalue=1,offvalue=0,variable=self.unliminted_act_flag)
        self.unlimited_act.grid(row=7,column=0,sticky=tk.W,columnspan=1)


        ##########  PROCESS OVERVIEW  ##########

        self.process_frame = tk.Frame(self,relief=tk.SUNKEN)
        self.process_frame.grid(row=0,column=1,padx=10,pady=10,sticky=tk.N+tk.W)

        labelframe_process = tk.LabelFrame(self.process_frame, text="Process Overview", padx=5, pady=5)
        labelframe_process.grid(row=0,column=0,padx=10,pady=10)

        # Actions sent
        self.label_actions_sent = tk.Label(labelframe_process, text="Actions Sent:")
        self.label_actions_sent.grid(row=0,column=0,sticky=tk.W)

        self.actions_sent_num = tk.Label(labelframe_process, text="0")
        self.actions_sent_num.grid(row=0,column=1,sticky=tk.W)


        # Actions per min
        self.label_actions_per_min = tk.Label(labelframe_process, text="Actions per min:")
        self.label_actions_per_min.grid(row=1,column=0,sticky=tk.W)

        self.actions_per_min_num = tk.Label(labelframe_process, text="-")
        self.actions_per_min_num.grid(row=1,column=1,sticky=tk.W)


        # Threads Running
        self.label_threads = tk.Label(labelframe_process, text="Threads Running:")
        self.label_threads.grid(row=2,column=0,sticky=tk.W)

        self.threads_num = tk.Label(labelframe_process, text="0")
        self.threads_num.grid(row=2,column=1,sticky=tk.W)


        # Proxies processed
        self.label_proxies_processed = tk.Label(labelframe_process, text="Proxies processed:")
        self.label_proxies_processed.grid(row=3,column=0,sticky=tk.W)

        self.proxies_processed_num = tk.Label(labelframe_process, text="0 / "+str(len(self.xdata.get_proxies())))
        self.proxies_processed_num.grid(row=3,column=1,sticky=tk.W)

        # Proxies to run
        self.label_proxies_run = tk.Label(labelframe_process, text="Proxies to run:")
        self.label_proxies_run.grid(row=4,column=0,sticky=tk.W)

        self.proxies_run_num = tk.Label(labelframe_process, text=str(len(self.xdata.get_proxies())))
        self.proxies_run_num.grid(row=4,column=1,sticky=tk.W)


        # Threads to Run
        self.label_threads_setting = tk.Label(labelframe_process, text="Threads to Run:")
        self.label_threads_setting.grid(row=5,column=0,sticky=tk.W)

        self.amount_of_threads = tk.IntVar()

        threads = ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14','15','16','17','18','19','20')
        self.threads_amount = tk.OptionMenu(labelframe_process, self.amount_of_threads, *threads)
        self.amount_of_threads.set(1)
        self.threads_amount.grid(row=5,column=1,sticky=tk.W)

        # Show browser
        self.browser_show_flag = tk.IntVar()
        self.browser_show_flag.set(1)
        self.show_browser_flag = tk.Checkbutton(labelframe_process, text="Show browser (safer but slower, more CPU expensive)",onvalue=1,offvalue=0,variable=self.browser_show_flag)
        self.show_browser_flag.grid(row=6,column=0,sticky=tk.W,columnspan=8)

        # Actions butttons
        self.start_process = tk.Button(labelframe_process, text="Start Process", command=lambda: self.start_voting())
        self.start_process.grid(row=7,column=0, sticky=tk.W)

        self.stop_process = tk.Button(labelframe_process, text="Stop Process", state=tk.DISABLED, command=lambda: self.stop_voting())
        self.stop_process.grid(row=7,column=1, sticky=tk.W)

        self.pause_process = tk.Button(labelframe_process, text="Pause Process",state=tk.DISABLED)
        self.pause_process.grid(row=7,column=2, sticky=tk.W)

    def autostart_process_config(self):
        # set delay vars
        self.delay_a.delete(0, tk.END)
        self.delay_a.insert(0, self.xdata.get_delay_from())
        self.delay_b.delete(0, tk.END)
        self.delay_b.insert(0, self.xdata.get_delay_to())

        # threads
        self.amount_of_threads.set(self.xdata.get_amount_threads())

        # captcha
        self.captcha_chosen.set(self.xdata.get_captcha_service())

        # continuos run mode
        self.autorestart_flag.set(self.xdata.get_continuos_run_mode())

        # browser show flag
        self.browser_show_flag.set(self.xdata.get_show_browser())
        self.browser_chosen.set("Firefox")

        self.start_voting()

    def stop_voting(self):
        self.reset_start_buttons()

        try:
            self.process.stop_process()
        except:
            self.logger.warn("Process stopped or error ocurred. Better to restart.")

    def reset_start_buttons(self):
        self.start_process['state'] = 'normal'
        self.stop_process['state'] = 'disabled'
        self.pause_process['state'] = 'disabled'
        self.threads_amount['state'] = 'normal'
        self.threads_num['text'] = "0"

    def disable_start_buttons(self):
        self.threads_amount['state'] = 'disabled'
        self.start_process['state'] = 'disabled'
        self.stop_process['state'] = 'normal'
        self.pause_process['state'] = 'normal'

    def reset_stats(self):
        self.actions_sent_num['text'] = "0"
        self.actions_per_min_num['text'] = "-"
        self.proxies_processed_num['text'] = "0 / "+str(len(self.xdata.get_proxies()))
        self.threads_num['text'] = "0"

    def start_voting(self):

        proxies = self.xdata.get_proxies()

        if self.amount_of_threads.get() > len(proxies):
            self.logger.warn("error: cannot have more threads than proxies")
        else:
             # disable buttons
            self.disable_start_buttons()
            self.reset_stats()

            delay = {
                'from': self.delay_a.get(),
                'to': self.delay_b.get()
            }

            captcha_api = "2captcha"

            if self.captcha_chosen.get() == "Antigate":
                captcha_api = "antigate"
            elif self.captcha_chosen.get() == "2Captcha":
                captcha_api = "2captcha"
            elif self.captcha_chosen.get() == "ImageTyperz":
                captcha_api = "imagetyperz"

            vote_urls = self.get_vote_url()

            self.process = XProcess(self.amount_of_threads.get(), delay, vote_urls, proxies, self,
                                    self.logger, self.autorestart_flag.get(),
                                    captcha_api, self.browser_chosen.get(), self.queue,
                                    self.browser_show_flag.get())

            t = Thread(target=self.process.start_process)
            t.start()

    def process_incoming(self):

        while self.queue.qsize():
            try:
                msg_update = self.queue.get(0)

                if "vote_count" in msg_update.get_type():
                    curr = int(self.actions_sent_num['text']) + 1
                    self.actions_sent_num['text'] = curr

                elif "proxy_processed" in msg_update.get_type():
                    self.proxies_processed_num['text'] = msg_update.get_value()

                elif "thread_count" in msg_update.get_type():
                    self.threads_num['text'] = str(msg_update.get_value())

                elif "reset_buttons" in msg_update.get_type():
                    if "not_running" in msg_update.get_value():
                        self.reset_start_buttons()

                self.update_credits_count()
            except Queue.Empty:
                pass

    def update_credits_count(self):
        try:
            print "updating credits count..."
            available = self.conn.get_available_credits(self.current_user.get_username())

            if int(available) < 0:
                available = "0"

            self.label_credits_num['text'] = ""+available
        except:
            print "couldnt update credits count..."
            pass

    def get_vote_url(self):

        if self.controller.app.current_user.get_type() == "admin":
            urls = self.vote_url.get("1.0", tk.END).splitlines()
            return urls[0]
        else:
            conn = XConnectDB()
            url = conn.get_vote_url(self.controller.app.current_user.get_config_id())
            return url
