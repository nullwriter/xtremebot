import requests

class ImageTyperz:

    def __init__(self, username, password):
        self.username = username
        self.pasword = password

        self.UPLOAD_CAPTCHA = "http://captchatypers.com/Forms/UploadGoogleCaptcha.ashx"
        self.GET_BALANCE = "http://captchatypers.com/Forms/RequestBalance.ashx"
        self.BAD_IMAGE = "http://captchatypers.com/Forms/SetBadImage.ashx"
        self.last_image_id = ""

    def get_balance(self):
        req = requests.post(self.GET_BALANCE,
                          data={'action':"REQUESTBALANCE",
                                'username': self.username, 'password': self.pasword})

        #print "status code = "+str(req.status_code)
        #print "reason = "+str(req.reason)

        if "ERROR" in req.text:
            raise Exception

        return req.text


    def solve(self, file):
        print "sending request upload captcha file"
        req = requests.post(self.UPLOAD_CAPTCHA,
                            data={'action':"UPLOADCAPTCHA",
                                  'username': self.username, 'password': self.pasword},
                            files={'file': open(file,'rb')})
        print "after sent request"
        print "status code = "+str(req.status_code)
        print "reason = "+str(req.reason)
        print "response = "+req.text

        if "ERROR" in req.text:
            raise Exception

        """ Saving the last image in case we need to request refund """
        data = req.text.split("|")
        self.last_image_id = data[0]

        return data[1]

    def bad_image(self):
        req = requests.post(self.UPLOAD_CAPTCHA,
                            data={'action':"UPLOADCAPTCHA",
                                  'username': self.username, 'password': self.pasword,
                                  'imageid': self.last_image_id})

        if "ERROR" in req.text:
            print "Requested refund but an error ocurred"
            return False

        if "SUCCESS" in req.text:
            print "Requested refund as captcha answer was wrong"
            return True
