import time

class TimeOutDriver:

    def __init__(self, driver, logger, timeout):
        self.timeout = timeout
        self.web_driver = driver
        self.answered = False
        self.logger = logger
        self.logger.warn("TimeOut: will timeout in "+str(self.timeout)+" seconds")

    def start_timer(self):
        count = self.timeout

        while count > 0:
            time.sleep(1)
            count -= 1

        # time's up
        if not self.answered:
            self.logger.warn("TimeOut: captcha timed out")
            self.web_driver.quit()

    def found_solution(self):
        self.answered = True