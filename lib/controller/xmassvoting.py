from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.proxy import *
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from lib.controller.xcaptcha import CaptchaSolve
from lib.model.xdata import XData
import time
import os
from sys import platform as _platform
import random
from threading import Thread


class MassVoting:

    def __init__(self, proxy_list, url, delay, process_master, log, thread_name, captcha_api,
                 captcha_key, captcha_user, captcha_pass, browser_chosen, lock, browser_show=False):

        self.process_master = process_master
        self.proxy_list = proxy_list

        self.delay = delay

        self.vote_done = 0
        self.vote_failed = 0

        self.thread_name = thread_name

        self.proxy_voted = []
        self.proxy_failed = []

        self.vote_url = url
        self.browser_show = browser_show
        self.log = log
        self.captcha_api = captcha_api
        self.browser_chosen = browser_chosen

        self.config = XData()
        self.lock = lock

        self.captcha_key = captcha_key
        self.captcha_user = captcha_user
        self.captcha_pass = captcha_pass
        self.limit_flag = False

        # define credits if client
        if self.process_master.current_user.get_type() == "client":
            self.limit_flag = True

    def process_manager(self):

        proxy_count = len(self.proxy_list)
        counter = 1
        self.vote_done = 0
        self.vote_failed = 0

        self.max_retry_proxy = self.config.get_max_retry_proxy()

        # for each proxy
        for proxy in self.proxy_list:

            # check if still has credits
            if not self.process_master.has_available_credits():
                self.log.warn("[" + self.thread_name + "] All credits were used, ending thread.")
                break

            credits = 1
            proxy_tries = 1

            while proxy_tries <= self.max_retry_proxy:

                try:
                    result = self.start_process(proxy)
                    if result:
                        break
                except Exception as e:
                    print "Exception occurred in process (non fatal): "+str(e)
                    result = False
                finally:
                    proxy_tries += 1
                    if proxy_tries <= self.max_retry_proxy and not result:
                        self.log.warn("["+self.thread_name+"] retrying proxy ("+str(proxy_tries)+" / "+str(self.max_retry_proxy)+")")
                credits += 1

            if result:
                self.vote_done += 1
                self.process_master.send_vote_done()
                self.proxy_voted.append(proxy)
            else:
                self.vote_failed += 1
                self.proxy_failed.append(proxy)

            if counter < proxy_count and result:
                sleep_time = random.randint(int(self.delay['from']), int(self.delay['to']))
                self.log.warn("["+self.thread_name+"] sleeping "+str(sleep_time)+" seconds until next vote")
                time.sleep(sleep_time)
            else:
                self.log.warn("["+self.thread_name+"] going straight to next proxy as previous didn't work")

            self.process_master.notify_proxy_processed()
            counter += 1

            if self.limit_flag:
                self.process_master.add_used_credit(credits)

        self.log.warn("["+self.thread_name+"] Process done. Votes: "+str(self.vote_done)+", Failed: "+str(self.vote_failed))
        self.process_master.notify_massvoting_done(self, self.vote_done, self.vote_failed)



    def stop_process(self):
        self.close_driver()

    def start_process(self, proxy):
        self.log.warn("["+self.thread_name+"] starting vote using proxy = "+proxy)

        result_captcha = False
        result_vote = False
        max_try = self.config.get_max_retry_proxy()
        strict_vote = self.config.get_strict_vote_check()  # deprecated
        tries = 0

        while not result_captcha:

            if tries >= max_try:
                self.log.warn("["+self.thread_name+"] max tries reached")
                break

            """
            Get driver
            """
            proxy = proxy
            self.lock.acquire()
            self.driver = self.get_driver(proxy)
            self.lock.release()

            """
            Initialize captcha solver
            """
            self.captcha = CaptchaSolve(self.log, self.captcha_api, self.captcha_key, self.captcha_user, self.captcha_pass)

            """
            Loading vote URL
            """
            self.log.warn("["+self.thread_name+"] getting = " + self.vote_url)
            try:
                # setting up the timeout
                self.load_site(self.driver, self.vote_url)
            except Exception as e:
                self.log.warn("["+self.thread_name+"] couldn't load correctly ("+str(e)+")")
                self.close_driver()
                return False

            """
            Check if site loaded correctly
            """
            try:
                loaded_correctly = self.check_correctly_loaded()
            except:
                self.close_driver()
                return False

            if loaded_correctly:
                try:
                    """
                    Solve Captcha
                    """
                    result_captcha = self.captcha.solve(self.driver, self.captcha_api)
                except:
                    # delete captcha image saved locally
                    self.captcha.clean_captcha_img()
                    self.close_driver()
                    return False

                # if captcha solved, contiue to vote
                if result_captcha:

                    # delete captcha image saved locally
                    self.captcha.clean_captcha_img()

                    print "solved captcha"
                    self.log.warn("["+self.thread_name+"] solved captcha")

                    """
                    Click on Vote
                    """
                    self.driver.switch_to.default_content()
                    click_vote_tries = 0
                    clicked_vote = False

                    while not clicked_vote:

                        if click_vote_tries > 10:
                            self.close_driver()
                            break
                        try:
                            self.driver.set_page_load_timeout(self.config.get_page_timeout_after_click_vote())
                            self.driver.find_element_by_xpath('//*[@id="middleb"]/div[1]/div/form/input[3]').click()
                            clicked_vote = True
                        except TimeoutException as e:
                            self.log.warn("[" + self.thread_name + "] after click vote timeout")
                            self.driver.execute_script("window.stop();")
                            clicked_vote = True
                        except Exception as e:
                            click_vote_tries += 1
                            print "couldnt click vote, retrying...("+str(e)+")"
                            time.sleep(0.5)

                    if clicked_vote:
                        """
                        After clicked on vote
                        """
                        print "apparently voted!"
                        self.log.warn("[" + self.thread_name + "] **** Vote Succeed ****!")
                        self.close_driver()
                        return True
                    else:
                        """
                        When driver couldnt click on vote
                        """
                        print "couldnt do vote"
                        self.log.warn("[" + self.thread_name + "] couldnt do vote")
                        self.driver.save_screenshot("no_vote.png")
                        result_captcha = False

                else:
                    result_vote = False
                    print "couldnt solve captcha. no vote"
                    self.log.warn("[" + self.thread_name + "] couldnt solve captcha. no vote")

                self.close_driver()
                tries += 1
            else:
                result_vote = False
                break

        try:
            self.close_driver()
        except Exception as e:
            print "At end of start_process() in xmassvoting: "+str(e)
            pass


        # delete captcha image saved locally
        self.captcha.clean_captcha_img()

        return result_vote

    def load_site(self, driver, url):
        try:
            driver.set_page_load_timeout(self.config.get_page_load_timeout())
            driver.get(url)
        except TimeoutException as e:
            self.log.warn("[" + self.thread_name + "] executed js script to stop loading (timeout)")
            driver.execute_script("window.stop();")

    def check_correctly_loaded(self):

        loaded = False
        loaded_max_tries = self.config.get_reload_page_404_tries()
        loaded_count = 0

        while not loaded:

            # loop breaker
            if loaded_count >= loaded_max_tries:
                self.log.warn(
                    "[" + self.thread_name + "] max refreshing site reached. looks like proxy is blocked by website.")
                self.close_driver()
                loaded = False
                return False

            # test for blocked proxy screen
            try:
                texto = self.driver.find_element_by_xpath('//body/div/div[2]/p[@class="red"]').text

                if "Request denied" in texto:
                    self.log.warn("[" + self.thread_name + "] proxy is blocked by website.")
                    if self.config.get_full_debug():
                        self.driver.save_screenshot("proxy_blocked"+str(random.randint(10000,9999))+".png")
                    self.close_driver()
                    return False
            except:
                pass

            # test for  "We apologize too many connections" screen
            try:
                texto = self.driver.find_element_by_xpath('//body/h3').text

                if "apologize" in texto:
                    loaded = False
                    self.log.warn("[" + self.thread_name + "] hasnt loaded")
                    if self.config.get_full_debug():
                        self.driver.save_screenshot("screensot_nobrowser_"+str(random.randint(10000,9999))+".png")
                    time.sleep(0.2)
                    self.driver.refresh()
            except:
                pass

            # test for "World of Warcraft top 100" screen (loaded correctly)
            try:
                texto = self.driver.find_element_by_xpath('//*[@align="left"]/div/h2').text

                if "Top 100" in texto:
                    print "loaded correctly"
                    self.log.warn("[" + self.thread_name + "] loaded correctly (1)")
                    loaded = True
                    return True
            except:
                self.log.warn("[" + self.thread_name + "] hasnt loaded (2)")
                loaded = False
                time.sleep(0.2)
                self.driver.refresh()
                if self.config.get_full_debug():
                    self.driver.save_screenshot("screensot_nobrowser_"+str(random.randint(10000,9999))+".png")

            loaded_count += 1

        return loaded

    def get_os(self):
        platform = "osx"

        if _platform == "linux" or _platform == "linux2":
            platform = "linux"
        elif _platform == "darwin":
            platform = "osx"
        elif _platform == "win32":
            platform = "windows"

        return platform

    def get_driver(self,proxy):

        curr_os = self.get_os()
        windows = False

        if "windows" in curr_os:
            windows = True


        if not self.browser_show:
            webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.settings.userAgent'] = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36'
            #webdriver.DesiredCapabilities.PHANTOMJS["phantomjs.page.settings.localToRemoteUrlAccessEnabled"] = True
            #webdriver.DesiredCapabilities.PHANTOMJS["phantomjs.page.settings.browserConnectionEnabled"] = True

            if windows:
                driver = webdriver.PhantomJS(service_args=['--ignore-ssl-errors=true','--proxy='+proxy,'--proxy-type=http'], executable_path="drivers/phantomjs.exe")
            else:
                driver = webdriver.PhantomJS(service_args=['--ignore-ssl-errors=true','--proxy='+proxy,'--proxy-type=http'])
            driver.set_window_size(1440, 900)
            #driver.maximize_window()
        else:

            # setup proxy
            chrome_options = webdriver.ChromeOptions()
            if self.config.get_use_proxy():
                chrome_options.add_argument('--proxy-server=%s' % proxy)
            chrome_options.add_argument('--lang=en')
            #chrome_options.add_argument("--incognito")

            if self.browser_chosen == "Firefox":
                browser_firefox = 1
            else:
                browser_firefox = 0

            if(windows):

                if browser_firefox:
                    myProxy = proxy

                    curr_proxy = Proxy({
                        'proxyType': ProxyType.MANUAL,
                        'httpProxy': myProxy,
                        'ftpProxy': myProxy,
                        'sslProxy': myProxy,
                        'noProxy': ''  # set this value as desired
                    })

                    firefoxbinary = "drivers/firefox.exe"
                    firebox_actual_binary = FirefoxBinary(firefoxbinary)

                    # if use_profile set, then get the folder of profile in /data/firefox_profile/folder
                    """
                    if self.config.get_use_firefox_profile():
                        path_to_profile = "data/firefox_profile/"

                        def get_immediate_subdirectories(a_dir):
                            return [name for name in os.listdir(a_dir)
                                    if os.path.isdir(os.path.join(a_dir, name))]

                        profile_file = get_immediate_subdirectories(path_to_profile)
                        path_to_profile = path_to_profile + profile_file[0]

                        firefox_profile = webdriver.FirefoxProfile(path_to_profile)
                    else:
                        firefox_profile = webdriver.FirefoxProfile()
                    """

                    firefox_profile = webdriver.FirefoxProfile()
                    firefox_profile.set_preference("intl.accept_languages", 'en-us')
                    firefox_profile.update_preferences()

                    if self.config.get_use_proxy():
                        driver = webdriver.Firefox(proxy=curr_proxy, firefox_profile=firefox_profile)
                    else:
                        driver = webdriver.Firefox( firefox_profile=firefox_profile)

                else:
                    chromedriver = "drivers/chromedriver.exe"
                    os.environ["webdriver.chrome.driver"] = chromedriver
                    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)

            # mac osx
            else:

                if browser_firefox:


                    webdriver.DesiredCapabilities.FIREFOX['proxy'] = {
                        "httpProxy":proxy,
                        "ftpProxy":proxy,
                        "sslProxy":proxy,
                        "noProxy":None,
                        "proxyType":"MANUAL",
                        "class":"org.openqa.selenium.Proxy",
                        "autodetect":False
                    }

                    # you have to use remote, otherwise you'll have to code it yourself in python to
                    driver = webdriver.Remote("http://localhost:4444/wd/hub", webdriver.DesiredCapabilities.FIREFOX)


                    self.driver = webdriver.Firefox(proxy=proxy)
                    self.driver.implicitly_wait(30)
                    self.base_url = "https://www.google.ie/"
                    self.verificationErrors = []
                    self.accept_next_alert = True

                else:
                    driver = webdriver.Chrome(chrome_options=chrome_options)
                    #driver = webdriver.Chrome()

        driver.implicitly_wait(1.5)
        return driver

    def close_driver(self):
        self.driver.quit()
