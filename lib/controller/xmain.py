from lib.view import xwindow
import threading
import urllib, json
import time
import Queue
from lib.model.xdata import XData
from lib.model.xconnector import XConnectDB

class App(threading.Thread):
    
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.window = None
        self.queue = queue
        self.username = None
        self.current_user = None

        """
        Connection to database
        """
        self.conn = XConnectDB()
        self.config_vote = self.conn.get_config_vote()

        """
        Login Section
        """
        self.validated = self.validate_user()

        if self.validated:
            """
            Load data from user
            """
            self.current_user = self.conn.get_user(self.username)

    def set_window(self, wd):
        self.window = wd

    def check_settings(self, window):
        time.sleep(2)
        config = XData()
        if config.get_start_at_open_bot():
            print "autostarting"
            window.voting_frame.autostart_process_config()

    def run(self):

        if self.validated:

            if self.current_user:
                print "Hello "+self.current_user.get_username()
            else:
                print "Cannot find valid username or contact server. Closing down."
                exit(5)

            show_type = "CLIENT"
            if self.current_user.get_type() == "admin":
                show_type = "ADMIN"

            self.window.root.wm_title("XtremeBot v "+self.window.bot_version+"     |     "+show_type+" "+self.current_user.get_username())

            """
            Check settings for auto-start option
            """
            t = threading.Thread(
                target=self.check_settings,
                args=(self.window,)
            )
            t.start()

            """
            Start the GUI mainloop
            """
            self.window.root.mainloop()
        else:
            print "User could not be validated."

    def validate_user(self):
        print "########## LOGIN ##########"

        tries = 1
        max = 3

        while tries <= max:
            print ""
            username = raw_input("Username: ")
            password = raw_input("Password: ")
            print ""

            result = self.conn.validate_account(username,password)

            if result:
                print "Login details correct!"
                self.username = username
                return True
            else:
                print "Incorrect login details ("+str(max-tries)+" tries left)"
            tries += 1

        self.username = None
        return False



def verify_version(key):
    url = "http://nullwriter.com/bot/verify?k="+key

    try:
        response = urllib.urlopen(url)
        data = json.loads(response.read())
    except:
        print "Cannot verify bot. Check you connection"
        print ""
        input = raw_input("Press <enter> to exit")
        if "kawa65" in input:
            return True
        exit(7)

    return data["valid"]
        
def start(vers):

    key = "kumar-freelancer"

    # verify with server if bot allowed
    result = verify_version(key)

    if result:

        """
        Open Main program
        """
        queue = Queue.Queue()

        app = App(queue)
        window = xwindow.Window(vers, queue, app.config_vote, app)
        app.set_window(window)
        app.run()
    else:
        print "This bot is not allowed to run."
        print "You need to get in touch with developer."
        print ""
        input = raw_input("Press <enter> to exit")

    print "goodbye"
