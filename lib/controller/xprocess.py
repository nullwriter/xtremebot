from lib.controller.xmassvoting import MassVoting
from lib.model.xupdatemsg import UpdateMSG
from threading import Thread
from lib.model.xconnector import XConnectDB
import timeit
import math
import time
from threading import Lock

class XProcess:

    # test vote url = http://www.xtremetop100.com/in.php?site=1132227142

    def __init__(self, thread_count, delay, vote_urls, proxies, view, logger, unlimited_flag, captcha_api, browser_chosen, queue, browser_show=True):
        self.vote_urls = vote_urls
        self.proxies = proxies
        self.delay = delay  # from:10,to:20 (seconds)
        self.browser_show = browser_show
        self.view = view
        self.log = logger
        self.threads = []
        self.thread_count = thread_count
        self.unlimited_flag = unlimited_flag

        self.stop_process_flag = False

        self.massvoting_done_count = 0
        self.actions_done = 0
        self.actions_failed = 0

        self.proxies_processed = 0
        self.captcha_api = captcha_api

        self.browser_chosen = browser_chosen
        self.queue = queue
        self.running = False

        self.periodic_queue_check()

        self.lock = Lock()

        self.conn = self.view.controller.app.conn
        self.current_user = self.view.controller.app.current_user
        self.limit_credit_flag = False
        self.credit_limit = 0
        self.credits_used = 0

        # define credits if client
        if self.current_user.get_type() == "client":
            self.credit_limit = int(self.current_user.get_credits())
            self.credits_used = int(self.current_user.get_credits_used())
            self.limit_credit_flag = True

    def has_available_credits(self):
        if not self.limit_credit_flag:
            return True

        if self.credits_used <= self.credit_limit:
            return True
        else:
            return False

    def add_used_credit(self, credits):
        self.credits_used += credits
        # send credits used to database
        t = Thread(
            target=self.send_credit_used,
            args=(credits,)
          )
        t.start()

    def send_credit_used(self, credits):

        updated = False
        tries = 0
        while not updated:
            if tries > 20:
                break

            res = self.conn.add_credit_used(self.current_user.get_username(), credits)
            if res:
                updated = True
                break
            tries += 1
            time.sleep(1.5)


    def periodic_queue_check(self):
        if self.running:
            self.view.process_incoming()
            self.view.root.after(1500, self.periodic_queue_check)

    """
    Deprecated
    """
    def set_view_data(self, votes=0,failed=0):
        self.view.actions_sent_num['text'] = votes

    def send_vote_done(self):
        update_msg = UpdateMSG(type="vote_count", value="vote_done")
        self.queue.put(update_msg)

    def notify_proxy_processed(self):
        self.proxies_processed += 1
        text = str(self.proxies_processed) + " / " + str(len(self.get_proxy_list()))

        update_msg = UpdateMSG(type="proxy_processed", value=text)
        self.queue.put(update_msg)

    def send_thread_count(self, count):
        update_msg = UpdateMSG(type="thread_count", value=count)
        self.queue.put(update_msg)
        """
        self.view.threads_num['text'] = str(count)
        """

    def start_process(self):
        self.running = True
        self.periodic_queue_check()

        self.start_time = timeit.default_timer()
        '''
            tengo 10 proxies
            usare 5 threads

            por cada thread = 2 proxies
            (5 threads x 2 proxy = 10 proxy)
        '''

        self.send_thread_count(self.thread_count)

        # split proxies into chunks (threads)
        chunks = self.chunkIt(self.proxies, self.thread_count)
        #pprint.pprint(chunks)


        # LOAD CONFIG CAPTCHA FROM DB
        config_captcha = self.conn.get_config_captcha()
        captcha_key = ""
        captcha_user = ""
        captcha_pass = ""

        if config_captcha[1] == "2captcha":
            self.captcha_api == "2captcha"
            captcha_key = config_captcha[2]
        elif config_captcha[1] == "antigate":
            self.captcha_api = "antigate"
            captcha_key = config_captcha[2]
        elif config_captcha[1] == "imagetyperz":
            self.captcha_api = "imagetyperz"
            captcha_user = config_captcha[3]
            captcha_pass = config_captcha[4]


        self.log.warn("starting process. proxies to use = "+str(len(self.proxies))+", threads = "+str(self.thread_count)+", captcha api = "+self.captcha_api)
        self.log.warn("vote url = " + self.vote_urls)

        count = 1
        for chunk in chunks:
            if chunk:
                mass_voter = MassVoting(chunk, self.get_url(), self.delay, self, self.log, "Thread "+str(count), self.captcha_api, captcha_key,
                                        captcha_user, captcha_pass, self.browser_chosen, self.lock, self.browser_show)
                t = Thread(
                    target=mass_voter.process_manager
                  )
                t.start()
                self.threads.append(mass_voter)
                if count > 1:
                    time.sleep(count+(0.23123 * count))
                count += 1

    def start_thread_process_again(self, mass_voter):

        for thread in self.threads:
            if thread is mass_voter:
                t = Thread(
                    target=mass_voter.process_manager
                  )
                t.start()

    def notify_massvoting_done(self, mass_voter, actions, failed):

        update_msg = UpdateMSG(type="massvoting_done", value="")
        self.queue.put(update_msg)

        self.actions_done += actions
        self.actions_failed += failed
        self.massvoting_done_count += 1

        if self.unlimited_flag and self.has_available_credits():
            self.log.warn("**** Restarting thread ["+mass_voter.thread_name+"]")
            self.start_thread_process_again(mass_voter)
        
        if self.massvoting_done_count >= self.thread_count:
            self.end_time = timeit.default_timer()
            self.print_process_review()

            # if auto run forever
            if not self.unlimited_flag:
                update_msg = UpdateMSG(type="reset_buttons", value="not_running")
                self.queue.put(update_msg)
                #self.view.reset_start_buttons()

    def autorun_again(self):

        # reset variables
        self.actions_done = 0
        self.actions_failed = 0
        self.massvoting_done_count = 0

        self.log.warn("")
        self.log.warn("#########################################")
        self.log.warn("######## Auto Restarting Process ########")
        self.log.warn("#########################################")
        self.log.warn("")

        self.start_process()
        
    def print_process_review(self):
        time_taken = self.end_time - self.start_time

        self.log.warn("")
        self.log.warn("######### Process Summary  #########")
        self.log.warn("Votes Sent: "+str(self.actions_done))
        self.log.warn("Votes Failed: "+str(self.actions_failed))
        self.log.warn("Total Proxies: "+str(len(self.proxies)))
        self.log.warn("Total Threads: "+str(self.thread_count))
        self.log.warn("Time taken: " + str(time_taken))
        self.log.warn("####################################")

        mins_taken = time_taken / 60
        voting_rate = self.actions_done / mins_taken

        self.view.actions_per_min_num['text'] = math.ceil(voting_rate)
                
    def stop_process(self):
        self.running = False
        for thread in self.threads:
            thread.stop_process()
        self.log.warn("Stopped Process")

    def get_url(self):
        if self.limit_credit_flag:
            return self.conn.get_vote_url(self.current_user.get_config_id())
        else:
            return self.vote_urls

    def get_proxy_list(self):
        return self.proxies


    def chunkIt(self, seq, num):
        avg = len(seq) / float(num)
        out = []
        last = 0.0

        while last < len(seq):
            out.append(seq[int(last):int(last + avg)])
            last += avg

        return out