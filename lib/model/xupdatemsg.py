
class UpdateMSG(object):

    def __init__(self, type, value, list_vals=None):
        self.type = type
        self.value = value
        self.list_vals = list_vals

    def get_value(self):
        return self.value

    def get_type(self):
        return self.type

    def get_list(self):
        return self.list_vals

    def add_list_obj(self, obj):
        self.list_vals.put(obj)