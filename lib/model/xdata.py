import json

class XData:
    
    def __init__(self):
        self.config_file = "data/settings.txt"

        with open(self.config_file) as json_file:
            json_data = json.load(json_file)

            self.data = json_data

    def get_proxies(self):
        return [line.rstrip('\n') for line in open('data/proxies.txt')]

    def get_2captcha_key(self):
        return self.data['2captcha']['key']

    def get_antigate_key(self):
        return self.data['antigate']['key']
        
    def get_max_retry_proxy(self):
        return self.data["mass_voting"]["max_retry_proxy"]
        
    def get_reload_page_404_tries(self):
        return self.data["mass_voting"]["reload_page_404_tries"]
        
    def get_max_retry_elements(self):
        return self.data["captcha_solver"]["max_retry_find_elements"]
        
    def get_strict_vote_check(self):
        return self.data["captcha_solver"]["strict_vote_check"]
        
    def get_retry_elements_delay(self):
        return self.data["captcha_solver"]["retry_elements_delay"]

    def get_vote_url(self):
        return self.data["mass_voting"]["url"]

    def get_firefox_browser_flag(self):
        return self.data["mass_voting"]["browser"]['firefox']

    def get_website_load_timeout(self):
        return self.data["mass_voting"]["website_load_timeout"]

    def get_use_firefox_profile(self):
        return self.data["mass_voting"]["browser"]["use_firefox_profile"]

    def get_retry_find_widget(self):
        return 1

    def get_page_load_timeout(self):
        return self.data["mass_voting"]["page_load_timeout"]

    def get_page_timeout_after_click_vote(self):
        return self.data["mass_voting"]["page_timeout_after_click_vote"]

    def get_timeout_after_click_vote_active(self):
        return self.data["mass_voting"]["timeout_after_click_vote_active"]

    def get_imagetyperz_user(self):
        return self.data["imagetyperz"]["username"]

    def get_imagetyperz_pass(self):
        return self.data["imagetyperz"]["password"]

    def get_full_debug(self):
        return self.data["full_debug"]

    def get_start_at_open_bot(self):
        return self.data["start_at_open_bot"]

    def get_delay_from(self):
        return self.data["delay_from"]

    def get_delay_to(self):
        return self.data["delay_to"]

    def get_amount_threads(self):
        return self.data["thread_count"]

    def get_captcha_service(self):
        return self.data["captcha_service"]

    def get_continuos_run_mode(self):
        return self.data["continuos_run_mode"]

    def get_show_browser(self):
        return self.data["show_browser"]

    def get_use_proxy(self):
        return self.data["mass_voting"]["use_proxy"]
