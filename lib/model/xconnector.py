from lib.model.xuser import XUser
import MySQLdb
from MySQLdb import OperationalError

class XConnectDB:

    def __init__(self):
        self.host = "xtremebotdb.db.10404034.hostedresource.com"
        self.user = "xtremebotdb"
        self.pwd = "KaW34JJ42!"
        self.db_name = "xtremebotdb"

        self.connect()

    def get_vote_url(self, id):
        sql = "SELECT * FROM config WHERE flag_active=1 AND id="+str(id)

        try:
            self.cursor.execute(sql)

            result = self.cursor.fetchone()
            return result[1]
        except Exception as e:
            print "Couldn't execute query: " + str(e)

        return None

    def get_user(self, username):
        sql = "SELECT * FROM users WHERE username='"+username+"'"

        try:
            self.cursor.execute(sql)

            result = self.cursor.fetchone()
            tmp_credits = str(result[5]).strip("L")
            tmp_used = str(result[6]).strip("L")
            return XUser(username=result[1], password=result[2], type=result[3], credits=tmp_credits, used=tmp_used, config_id=result[7])
        except Exception as e:
            print "Couldn't execute query: "+str(e)

        return False

    def get_credits(self, user):
        sql = "SELECT 'credits' FROM users WHERE username='" + user + "'"

        try:
            self.cursor.execute(sql)
            result = self.cursor.fetchone()
            tmp_credits = str(result[0]).strip("L")
            return tmp_credits
        except Exception as e:
            print "Couldn't execute query: " + str(e)

        return False

    def get_credits_used(self, user):
        sql = "SELECT 'credits_used' FROM users WHERE username='" + user + "'"

        try:
            self.cursor.execute(sql)
            result = self.cursor.fetchone()
            tmp_credits = str(result[0]).strip("L")
            return tmp_credits
        except Exception as e:
            print "Couldn't execute query: " + str(e)

        return False

    def block_user(self, username):

        sql = "UPDATE users SET flag_active=0 WHERE username='"+username+"'"

        try:
            self.cursor.execute(sql)
            self.db.commit()
            return True
        except Exception as e:
            print "Couldn't execute update query: "+str(e)

        return False

    def unblock_user(self, username):

        sql = "UPDATE users SET flag_active=1 WHERE username='"+username+"'"

        try:
            self.cursor.execute(sql)
            self.db.commit()
            return True
        except Exception as e:
            print "Couldn't execute update query: "+str(e)

        return False

    def add_account(self, user, pwd, type="client"):

        sql = "INSERT INTO users(id, username, password, type_acc, flag_active) VALUES (null,'"+user+"','"+pwd+"','"+type+"', 1)"

        try:
            self.cursor.execute(sql)
            self.db.commit()
            return True
        except Exception as e:
            print "Couldn't execute query: "+str(e)

        return False

    def add_configuration(self, vote_url, title="None"):
        sql = "INSERT INTO config(id, vote_url, title, flag_active) VALUES (null,'"+vote_url+"','"+title+"', 1)"

        try:
            self.cursor.execute(sql)
            self.db.commit()
            return True
        except Exception as e:
            print "Couldn't execute query: "+str(e)

        return False

    def validate_account(self, user, pwd):
        sql = "SELECT * FROM users WHERE username='"+user+"' AND password='"+pwd+"' AND flag_active=1"

        try:
            self.cursor.execute(sql)

            result = self.cursor.rowcount
            if result > 0:
                return True
        except Exception as e:
            print "Couldn't execute query: "+str(e)

        return False

    def get_config_vote(self):
        sql = "SELECT * FROM config WHERE flag_active=1"

        try:
            self.cursor.execute(sql)

            result = self.cursor.fetchone()
            return result[1]
        except Exception as e:
            print "Couldn't execute query: "+str(e)

        return False

    def get_config_captcha(self):
        sql = "SELECT * FROM captcha_service WHERE flag_active=1"

        try:
            self.cursor.execute(sql)
            result = self.cursor.fetchone()
            return result
        except Exception as e:
            print "Couldn't execute query: " + str(e)

        return False

    def add_credit_used(self, username, used):
        sql = "UPDATE users SET credits_used = credits_used + "+str(used)+" WHERE username='"+username+"'"

        try:
            self.cursor.execute(sql)
            self.db.commit()
            return True
        except OperationalError as e:
            if 'MySQL server has gone away' in str(e):
                self.connect()
        except Exception as e:
            print "Couldn't execute update credits used query: "+str(e)

        return False

    def get_available_credits(self, user):
        sql = "SELECT credits-credits_used AS available FROM users WHERE username='"+user+"'"

        try:
            self.cursor.execute(sql)
            result = self.cursor.fetchone()
            tmp_credits = str(result[0]).strip("L")
            return tmp_credits
        except Exception as e:
            print "Couldn't execute query: " + str(e)

        return False

    def connect(self):
        try:
            # Open database connection
            self.db = MySQLdb.connect(self.host, self.user, self.pwd, self.db_name)

            # prepare a cursor object using cursor() method
            self.cursor = self.db.cursor()

        except Exception as e:
            print "Couldn't connect to database: " + str(e)
            exit(77)

    def close_connection(self):
        self.db.close()