

class XUser:

    def __init__(self, username, password, type, credits, used, config_id):

        self.username = username
        self.password = password
        self.type_acc = type
        self.credits = credits
        self.credits_used = used
        self.config_id = config_id

    def get_username(self):
        return self.username
    def get_password(self):
        return self.password
    def get_type(self):
        return self.type_acc
    def get_credits(self):
        return self.credits
    def get_credits_used(self):
        return self.credits_used
    def get_config_id(self):
        return  self.config_id

    def set_credits_used(self, num):
        self.credits_used = num

    def get_available_credits(self):
        num = int(self.credits) - int(self.credits_used)
        if num < 0:
            num = 0
        return num