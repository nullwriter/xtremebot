from lib.controller import xmain
import sys
from lib.controller.ImageTyperz import ImageTyperz
import Queue
from threading import Thread
import time
# start date = 17/03/2016
# last update = 08/04/16
# current version 1.0.1d

def get_version():
    return "1.0.1da"

def main(argv):
    print "XtremeBot v "+get_version()
    print "Close this window o press CTRL + C if you need to force close the bot."
    print ""
    xmain.start(get_version())


if __name__ == "__main__":
    main(sys.argv)